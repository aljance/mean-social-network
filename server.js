// imports
const express = require('express');
const path = require('path');
const body_parser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

// declare routes
const posts = require('./routes/posts');
const pictures = require('./routes/pictures');
const comments = require('./routes/comments');
const chat = require('./routes/chat');
const friends = require('./routes/friends');
const users = require('./routes/users');
const notif = require('./routes/notifications');
// server to be used
const app = express();

// port to be used
const port = 3000;

app.use(cors());

app.use(express.static(path.join(__dirname,'public')));

app.use(body_parser.json());

app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

// register routes
app.use('/api/posts',posts);
app.use('/api/comments',comments);
app.use('/api/pictures',pictures);
app.use('/api/friends',friends);
app.use('/api/chat',chat);
app.use('/api/users',users);
app.use('/api/notif',notif);
// default endpoint
app.get('/',(req,res)=>{
  res.send('Invalid Endpoint');
});

app.listen(port,()=>{
  console.log("Server started at post: "+port);
});
