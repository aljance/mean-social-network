module.exports = (sequelize,type,User,User1)=>{
  return sequelize.define('chat',{
    id:{
      type:type.INTEGER,
      primaryKey:true,
      autoIncrement:true
    },
    sender_id:{
      type:type.INTEGER,
      references:{
        model:User,
        key:'id'
      }
    },
    receiver_id:{
      type:type.INTEGER,
      references:{
        model:User1,
        key:'id'
      }
    },
    deleted:{
      type:type.BOOLEAN,
      defaultValue:false
    }
  },{
    timestamps:false
  });
};
