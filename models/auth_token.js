const moment = require('moment');
module.exports = (sequelize,type,User)=>{
  return sequelize.define('auth_token',{
    id:{
      type:type.INTEGER,
      primaryKey:true,
      autoIncrement:true
    },
    token:{
      type:type.STRING,
      allowNull:false,
      validate:{
        notEmpty:true
      }
    },
    user_id:{
      type:type.INTEGER,
      references:{
        model:User,
        key:'id'
      }
    },
    is_valid:{
      type:type.BOOLEAN,
      defaultValue:true
    },
    issue_date:{
      type:type.DATE,
      defaultValue:type.NOW
    },
    expiration_date:{
      type:type.DATE,
      defaultValue:moment().add(1,'days').toDate()
    }
  },{
    timestamps:false
  });
};
