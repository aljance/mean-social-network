module.exports = (sequelize,type,User,User1)=>{
  return sequelize.define('friend_req',{
    id:{
      type:type.INTEGER,
      primaryKey:true,
      autoIncrement:true
    },
    owner_id:{
      type:type.INTEGER,
      references:{
        model:User,
        key:'id'
      }
    },
    friend_id:{
      type:type.INTEGER,
      references:{
        model:User1,
        key:'id'
      }
    },
    upload_date:{
      type:type.DATE,
      defaultValue:type.NOW
    },
    deleted:{
      type:type.BOOLEAN,
      defaultValue:false
    },
    confirmed:{
      type:type.BOOLEAN,
      defaultValue:false
    }
  },{
    timestamps:false
  });
};
