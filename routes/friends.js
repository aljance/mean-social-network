const express = require('express');
const router = express.Router();
const friend_controller = require('../controllers/friends');
const passport = require('passport');

router.get('/request/all/:offset',passport.authenticate('jwt',{session:false}),friend_controller.get_all_friend_request);

router.get('/:userId/all/:offset',passport.authenticate('jwt',{session:false}),friend_controller.get_all_friends);

router.post('/add',passport.authenticate('jwt',{session:false}),friend_controller.send_friend_req);

router.post('/accept',passport.authenticate('jwt',{session:false}),friend_controller.accept_friend_req);

router.post('/decline',passport.authenticate('jwt',{session:false}),friend_controller.decline_frienq_req);

router.post('/cancel',passport.authenticate('jwt',{session:false}),friend_controller.decline_frienq_req);

router.post('/delete',passport.authenticate('jwt',{session:false}),friend_controller.delete_friend);


module.exports = router;
