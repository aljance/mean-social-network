const express = require('express');
const router = express.Router();
const chat_controller = require('../controllers/chat');
const passport = require('passport');

router.get('/all/chats/:offset',passport.authenticate('jwt',{session:false}),chat_controller.get_all_chats);

router.get('/all/friends/:offset',passport.authenticate('jwt',{session:false}),chat_controller.get_all_friends);

router.get('/all/:friend_id/:offset',passport.authenticate('jwt',{session:false}),chat_controller.get_all_message_from_chat);

router.post('/send',passport.authenticate('jwt',{session:false}),chat_controller.send_message);

module.exports = router;
