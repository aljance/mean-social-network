const express = require('express');
const router = express.Router();
const user_controller = require('../controllers/users');
const passport = require('passport');

router.post('/register',user_controller.register);

router.post('/login',user_controller.login);

router.get('/logout',passport.authenticate('jwt',{session:false}),user_controller.logout);

router.get('/forgot_password',user_controller.forgot_password);

router.get('/:userId/info',passport.authenticate('jwt',{session:false}),user_controller.user_info);

router.post('/update',passport.authenticate('jwt',{session:false}),user_controller.update_info);

router.post('/delete',passport.authenticate('jwt',{session:false}),user_controller.delete_user);

router.get('/check_token',passport.authenticate('jwt',{session:false}),user_controller.check_token);

router.get('/find/:search/:offset',passport.authenticate('jwt',{session:false}),user_controller.find_user);

router.post('/password/change',passport.authenticate('jwt',{session:false}),user_controller.change_password);

module.exports = router;
