const express = require('express');
const router = express.Router();
const pic_controller = require('../controllers/pictures');
const passport = require('passport');

router.get('/:userId/all/:offset',passport.authenticate('jwt',{session:false}),pic_controller.get_all_pics);

router.get('/:userId/profile_pic',passport.authenticate('jwt',{session:false}),pic_controller.get_profile_pic);

router.post('/profile_pic',passport.authenticate('jwt',{session:false}),pic_controller.set_profile_pic);

router.post('/create',passport.authenticate('jwt',{session:false}),pic_controller.create_pic);

router.post('/delete',passport.authenticate('jwt',{session:false}),pic_controller.delete_pic);

router.post('/like',passport.authenticate('jwt',{session:false}),pic_controller.like_pic);

router.get('/:picId/comments/all/:offset',passport.authenticate('jwt',{session:false}),pic_controller.get_pic_comments);

router.post('/comments',passport.authenticate('jwt',{session:false}),pic_controller.comment_pic);

router.post('/comments/like',passport.authenticate('jwt',{session:false}),pic_controller.like_comment_pic);

router.post('/comments/delete',passport.authenticate('jwt',{session:false}),pic_controller.delete_comment_pic);
//optional add picture_comment model and allow comments for pics

module.exports = router;
