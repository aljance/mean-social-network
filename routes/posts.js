const express = require('express');
const router = express.Router();
const post_controller = require('../controllers/posts');
const passport = require('passport');

router.get('/all/:offset',passport.authenticate('jwt',{session:false}),post_controller.get_all_posts);

router.get('/:postId',passport.authenticate('jwt',{session:false}),post_controller.get_post);

router.get('/user/:userId/:offset',passport.authenticate('jwt',{session:false}),post_controller.get_post_for_user);

router.post('/create',passport.authenticate('jwt',{session:false}),post_controller.create_post);

router.post('/update',passport.authenticate('jwt',{session:false}),post_controller.update_post);

router.post('/delete',passport.authenticate('jwt',{session:false}),post_controller.delete_post);

router.post('/like',passport.authenticate('jwt',{session:false}),post_controller.like_post);

module.exports = router;
