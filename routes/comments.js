const express = require('express');
const router = express.Router();
const comment_controller = require('../controllers/comments');
const passport = require('passport');

//only handles comments for posts pictures are handled in pictures route
router.get('/:postId/all/:offset',passport.authenticate('jwt',{session:false}),comment_controller.get_all_comments);

router.post('/create',passport.authenticate('jwt',{session:false}),comment_controller.create_comment);

router.post('/update',passport.authenticate('jwt',{session:false}),comment_controller.update_comment);

router.post('/delete',passport.authenticate('jwt',{session:false}),comment_controller.delete_comment);

router.post('/like',passport.authenticate('jwt',{session:false}),comment_controller.like_comment);

module.exports = router;
