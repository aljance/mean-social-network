const express = require('express');
const router = express.Router();
const notif_controller = require('../controllers/notifications');
const passport = require('passport');

router.get('/all/:offset',passport.authenticate('jwt',{session:false}),notif_controller.get_all_notif);

router.post('/update',passport.authenticate('jwt',{session:false}),notif_controller.update);

module.exports = router;
