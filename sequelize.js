const Sequelize = require('sequelize');
const config = require('./config/database');
const UserModel = require('./models/user');
const PostModel = require('./models/post');
const FriendReqModel = require('./models/friend_req');
const PictureModel = require('./models/picture');
const ChatModel = require('./models/chat');
const CommentModel = require('./models/comment');
const NotificationModel = require('./models/notification');
const AuthTokenModel = require('./models/auth_token');
const LikeModel = require('./models/like');
const FriendModel = require('./models/friend');
const LikeCommentModel = require('./models/like_comment');
const LikePictureModel = require('./models/like_picture');
const CommentPictureModel = require('./models/comment_pic');
const LikeCommentPicModel = require('./models/like_comment_pic');
const ChatMessageModel = require('./models/chat_message');

const sequelize = new Sequelize(config.database,config.database_username,config.database_password,{
  host:'localhost',
  dialect:'mysql',
  pool:{
    max:10,
    min:0,
    acquire:30000,
    idle:10000
  }
});

const User = UserModel(sequelize,Sequelize);
const Post = PostModel(sequelize,Sequelize,User);
const FriendReq = FriendReqModel(sequelize,Sequelize,User,User);
const Picture = PictureModel(sequelize,Sequelize,User);
const Chat = ChatModel(sequelize,Sequelize,User,User);
const Comment = CommentModel(sequelize,Sequelize,User,Post);
const Notification = NotificationModel(sequelize,Sequelize,User);
const AuthToken = AuthTokenModel(sequelize,Sequelize,User);
const Like = LikeModel(sequelize,Sequelize,User,Post);
const Friend = FriendModel(sequelize,Sequelize,User,User);
const LikeComment = LikeCommentModel(sequelize,Sequelize,User,Comment);
const LikePicture = LikePictureModel(sequelize,Sequelize,User,Picture);
const CommentPic = CommentPictureModel(sequelize,Sequelize,User,Picture);
const LikeCommentPic =LikeCommentPicModel(sequelize,Sequelize,User,CommentPic);
const ChatMessage = ChatMessageModel(sequelize,Sequelize,Chat,User);
// setting force to true will drop tables on each sync
sequelize.sync({force:false}).then(()=>{
  console.log('Database and tables created!');
});

module.exports = {
  User,Post,FriendReq,Picture,Chat,Comment,Notification,AuthToken,Like,Friend,sequelize,LikeComment,LikePicture,CommentPic,LikeCommentPic,ChatMessage
}
