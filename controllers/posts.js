const {User,Post,FriendReq,Picture,Chat,Comment,Notification,AuthToken,Like,Friend,sequelize} = require('../sequelize');
const jwt = require('jsonwebtoken');

exports.get_all_posts = function(req,res){
  const offset = parseInt(req.params.offset);
  const limit = 6;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const user_id = decode.id;
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date < new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        sequelize.query('SELECT p.id,p.user_id,p.text,p.upload_date,p.deleted,p.like_count,p.comment_count FROM posts p JOIN( SELECT friend_id FROM friends WHERE owner_id=? UNION SELECT owner_id FROM friends WHERE friend_id=? UNION SELECT ? FROM DUAL) m ON p.user_id = m.friend_id JOIN users u on p.user_id=u.id WHERE p.deleted=0 ORDER BY p.id DESC LIMIT ?, ?',
      {replacements:[user_id,user_id,user_id,offset,limit],type:sequelize.QueryTypes.SELECT}).then(results=>{
        if(results.length==0){
          res.json({'success':false,'error':'No posts to show'});
        }else{
          var counter = 0;
          var results_ar = [];
          results.forEach((obj)=>{
            Like.findOne({where:{user_id:decode.id,post_id:obj.id,active:true}}).then(like=>{
              if(like==null){
                User.findOne({where:{id:obj.user_id,active:true}}).then(u=>{
                  if(u==null){
                    console.log('user is null');
                    const add = {
                      'id':obj.id,
                      'user_id':obj.user_id,
                      'first_name':null,
                      'last_name':null,
                      'profile_pic':null,
                      'upload_date':(obj.upload_date).toDateString(),
                      'deleted':obj.deleted,
                      'like_count':obj.like_count,
                      'comment_count':obj.comment_count,
                      'view_comment':false,
                      'liked':false,
                      'text':obj.text,
                      'email':null
                    };
                    results_ar.push(add);
                    counter++;
                    if(counter==results.length){
                      res.json({'success':true,'results':results_ar});
                    }
                  }else{
                    console.log('user is not null');
                    Picture.findOne({where:{user_id:obj.user_id,is_profile_pic:true,deleted:false}}).then(pic=>{
                      if(pic==null){
                        console.log('user is not null but profile pic is null');
                        const add = {
                          'id':obj.id,
                          'user_id':obj.user_id,
                          'first_name':u.first_name,
                          'last_name':u.last_name,
                          'profile_pic':null,
                          'upload_date':(obj.upload_date).toDateString(),
                          'deleted':obj.deleted,
                          'like_count':obj.like_count,
                          'comment_count':obj.comment_count,
                          'view_comment':false,
                          'liked':false,
                          'text':obj.text,
                          'email':u.email
                        };
                        results_ar.push(add);
                        counter++;
                        if(counter==results.length){
                          res.json({'success':true,'results':results_ar});
                        }
                      }else{
                        console.log('user is not null and profile pic is not null');
                        const add = {
                          'id':obj.id,
                          'user_id':obj.user_id,
                          'first_name':u.first_name,
                          'last_name':u.last_name,
                          'profile_pic':pic.url,
                          'upload_date':(obj.upload_date).toDateString(),
                          'deleted':obj.deleted,
                          'like_count':obj.like_count,
                          'comment_count':obj.comment_count,
                          'view_comment':false,
                          'liked':false,
                          'text':obj.text,
                          'email':u.email
                        };
                        results_ar.push(add);
                        counter++;
                        if(counter==results.length){
                          res.json({'success':true,'results':results_ar});
                        }
                      }
                    });
                  }
                });
              }else{
                User.findOne({where:{id:obj.user_id,active:true}}).then(u=>{
                  if(u==null){
                    console.log('user is null and post is liked');
                    const add = {
                      'id':obj.id,
                      'user_id':obj.user_id,
                      'first_name':null,
                      'last_name':null,
                      'profile_pic':null,
                      'upload_date':(obj.upload_date).toDateString(),
                      'deleted':obj.deleted,
                      'like_count':obj.like_count,
                      'comment_count':obj.comment_count,
                      'view_comment':false,
                      'liked':true,
                      'text':obj.text,
                      'email':null
                    };
                    results_ar.push(add);
                    counter++;
                    if(counter==results.length){
                      res.json({'success':true,'results':results_ar});
                    }
                  }else{
                    Picture.findOne({where:{user_id:obj.user_id,is_profile_pic:true,deleted:false}}).then(pic=>{
                      if(pic==null){
                        console.log('user is not null, profile pic is null and post is liked');
                        const add = {
                          'id':obj.id,
                          'user_id':obj.user_id,
                          'first_name':u.first_name,
                          'last_name':u.last_name,
                          'profile_pic':null,
                          'upload_date':(obj.upload_date).toDateString(),
                          'deleted':obj.deleted,
                          'like_count':obj.like_count,
                          'comment_count':obj.comment_count,
                          'view_comment':false,
                          'liked':true,
                          'text':obj.text,
                          'email':u.email
                        };
                        results_ar.push(add);
                        counter++;
                        if(counter==results.length){
                          res.json({'success':true,'results':results_ar});
                        }
                      }else{
                        const add = {
                          'id':obj.id,
                          'user_id':obj.user_id,
                          'first_name':u.first_name,
                          'last_name':u.last_name,
                          'profile_pic':pic.url,
                          'upload_date':(obj.upload_date).toDateString(),
                          'deleted':obj.deleted,
                          'like_count':obj.like_count,
                          'comment_count':obj.comment_count,
                          'view_comment':false,
                          'liked':true,
                          'text':obj.text,
                          'email':u.email
                        };
                        console.log('user is not null, profile pic is not null, and post is liked');
                        results_ar.push(add);
                        counter++;
                        if(counter==results.length){
                          res.json({'success':true,'results':results_ar});
                        }
                      }
                    });
                  }
                });
              }
            });
          });
        }
      });
      }
    }
  });
};

exports.create_post = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const txt = req.body.post_text;

  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date<new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        User.findOne({where:{id:decode.id,active:true}}).then(r=>{
          if(r==null){
            res.json({'success':false,'error':'unexisting user'});
          }else{
            const post = Post.build({
              user_id:r.id,
              text:txt
            });
            post.save().then(()=>{
              Friend.findAll({where:{owner_id:decode.id}}).then(friends=>{
                if(friends==null){
                  res.json({'success':true,'message':'posted successfully but user has no friends'});
                }else{
                  const txt = "User "+r.first_name+" "+r.last_name+" has posted something new";
                  var counter=0;
                  friends.forEach((obj)=>{
                    const notif = Notification.build({user_id:obj.owner_id,text:txt});
                    notif.save().then(()=>{
                      console.log('saved');
                    });
                    counter++;
                    console.log(counter);
                    if(counter==friends.length){
                      res.json({'success':true,'message':'posted successfully'});
                    }
                  });
                }
              });
            }).catch(err=>{
              res.json({'success':false,'error':err});
            });
          }
        });
      }
    }
  });
}

exports.get_post_for_user = function(req,res){
  const uid = req.params.userId;
  const offset = parseInt(req.params.offset);
  const limit = 6;

  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);

  User.findOne({where:{email:uid,active:true}}).then(u=>{
    if(u==null){
      res.json({'success':false,'error':'Invalid User Id'});
    }else{
      const user_id = u.id;
      AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
        if(tok==null){
          res.json({'success':false,'error':'Invalid Token'});
        }else{
          if(tok.expiration_date < new Date()){
            tok.update({is_valid:false}).then(()=>{
              res.json({'success':false,'error':'Invalid Token'});
            });
          }else{
            Picture.findOne({where:{user_id:u.id,deleted:false,is_profile_pic:true}}).then(pic=>{
              if(pic==null){
                Post.findAll({
                  where:{'user_id':user_id,'deleted':false},
                  limit:limit,
                  offset:offset
                }).then(results=>{
                  if(results.length==0){
                    res.json({'success':false,'error':'This user has no posts'});
                  }else{
                    var counter = 0;
                    var ar = [];
                    results.forEach((obj)=>{
                      Like.findOne({where:{user_id:decode.id,active:true}}).then(like=>{
                        if(like==null){
                          const add = {
                            'id':obj.id,
                            'user_id':obj.user_id,
                            'first_name':u.first_name,
                            'last_name':u.last_name,
                            'profile_pic':null,
                            'upload_date':(obj.upload_date).toDateString(),
                            'deleted':obj.deleted,
                            'like_count':obj.like_count,
                            'comment_count':obj.comment_count,
                            'view_comment':false,
                            'liked':false,
                            'text':obj.text,
                            'email':u.email
                          };
                          ar.push(add);
                          counter++;
                          if(counter==results.length){
                            res.json({'success':true,'results':ar});
                          }
                        }else{
                          const add = {
                            'id':obj.id,
                            'user_id':obj.user_id,
                            'first_name':u.first_name,
                            'last_name':u.last_name,
                            'profile_pic':null,
                            'upload_date':(obj.upload_date).toDateString(),
                            'deleted':obj.deleted,
                            'like_count':obj.like_count,
                            'comment_count':obj.comment_count,
                            'view_comment':false,
                            'liked':true,
                            'text':obj.text,
                            'email':u.email
                          };
                          ar.push(add);
                          counter++;
                          if(counter==results.length){
                            res.json({'success':true,'results':ar});
                          }
                        }
                      });
                    });
                  }
                });
              }else{
                Post.findAll({
                  where:{'user_id':user_id,'deleted':false},
                  limit:limit,
                  offset:offset
                }).then(results=>{
                  if(results.length==0){
                    res.json({'success':false,'error':'This user has no posts'});
                  }else{
                    var counter = 0;
                    var ar = [];
                    results.forEach((obj)=>{
                      Like.findOne({where:{user_id:decode.id,active:true}}).then(like=>{
                        if(like==null){
                          const add = {
                            'id':obj.id,
                            'user_id':obj.user_id,
                            'first_name':u.first_name,
                            'last_name':u.last_name,
                            'profile_pic':pic.url,
                            'upload_date':(obj.upload_date).toDateString(),
                            'deleted':obj.deleted,
                            'like_count':obj.like_count,
                            'comment_count':obj.comment_count,
                            'view_comment':false,
                            'liked':false,
                            'text':obj.text,
                            'email':u.email
                          };
                          ar.push(add);
                          counter++;
                          if(counter==results.length){
                            res.json({'success':true,'results':ar});
                          }
                        }else{
                          const add = {
                            'id':obj.id,
                            'user_id':obj.user_id,
                            'first_name':u.first_name,
                            'last_name':u.last_name,
                            'profile_pic':pic.url,
                            'upload_date':(obj.upload_date).toDateString(),
                            'deleted':obj.deleted,
                            'like_count':obj.like_count,
                            'comment_count':obj.comment_count,
                            'view_comment':false,
                            'liked':true,
                            'text':obj.text,
                            'email':u.email
                          };
                          ar.push(add);
                          counter++;
                          if(counter==results.length){
                            res.json({'success':true,'results':ar});
                          }
                        }
                      });
                    });
                  }
                });
              }
            });
          }
        }
      });
    }
  });
};

exports.get_post = function(req,res){
  const post_id = req.params.postId;

  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);

  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date<new Date()){
      res.json({'success':false,'error':'Invalid Token'});
    }
    else{
      Post.findOne({where:{id:post_id,deleted:false}}).then(post=>{
        if(post==null){
          res.json({'success':false,'error':'Invalid post id'});
        }else{
          User.findOne({where:{id:post.user_id}}).then(user=>{
            if(user==null){
              res.json({'success':false,'error':'invalid user'});
            }else{
              res.json({
                'success':true,
                'text':post.text,
                'like_count':post.like_count,
                'comment_count':post.comment_count,
                'first_name':user.first_name,
                'last_name':user.last_name,
                'user_id':user.id
              });
            }
          });
        }
      });
    }
  });
}

exports.update_post = function(req,res){
  const post_id = parseInt(req.body.post_id);
  const text = req.body.text;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);

  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date < new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        Post.findOne({where:{'id':post_id,deleted:false}}).then(post=>{
          if(post==null){
            res.json({'success':false,'error':'invalid post id'});
          }else if(post.user_id!=decode.id){
            res.json({'success':false,'error':'Invalid user'});
          }else{
            post.update({'text':text}).then(()=>{
              res.json({'success':true,'message':'Post Updated'});
            });
          }
        });
      }
    }
  });
};

exports.delete_post = function(req,res){
  const post_id = req.body.post_id;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);

  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date<new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        Post.findOne({where:{id:post_id,deleted:false}}).then(post=>{
          if(post==null){
            res.json({'success':false,'error':'Invalid post id'});
          }else if(post.user_id != decode.id){
            res.json({'success':false,'error':'Invalid user'});
          }else{
            post.update({'deleted':true}).then(()=>{
              res.json({'success':true,'message':'Post Deleted'});
            });
          }
        });
      }
    }
  });
};

exports.like_post = function(req,res){
  const post_id = parseInt(req.body.post_id);
  console.log(req.body);
  console.log(post_id);
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);

  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date < new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        Post.findOne({where:{id:post_id,deleted:false}}).then(post=>{
          if(post==null){
            res.json({'success':false,'error':'Invalid Post id'});
          }else{
            Like.findOne({where:{post_id:post_id,user_id:decode.id,active:true}}).then(like=>{
              if(like==null){
                console.log('like is null');
                const lk = Like.build({
                  post_id:post_id,
                  user_id:decode.id
                });
                lk.save().then(()=>{
                  const like_count = post.like_count + 1;
                  post.update({'like_count':like_count}).then(()=>{
                      res.json({'success':true,'liked':true});
                  }).catch(err=>{
                    res.json({'success':false,'error':err});
                  });
                }).catch(err=>{
                  res.json({'success':false,'error':err});
                });
              }else{
                console.log('like is not null');
                like.update({'active':false}).then(()=>{
                  post.update({'like_count':post.like_count-1}).then(()=>{
                    res.json({'success':true,'liked':false});
                  });
                });
              }
            });
          }
        });
      }
    }
  });
};
