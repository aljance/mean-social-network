const {User,Post,FriendReq,Picture,Chat,Comment,Notification,AuthToken,Like,Friend,sequelize} = require('../sequelize');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

exports.change_password = function(req,res){
  const old_password = req.body.old_password;
  const new_password = req.body.new_password;
  const confirm_password = req.body.confirm_password;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  if(old_password==new_password){
    res.json({'success':false,'error':'New password can\'t be old password'});
  }else if(new_password!=confirm_password){
    res.json({'success':false,'error':'Passwords do not match'});
  }else{
    AuthToken.findOne({where:{token:token,is_valid:true,user_id:decode.id}}).then(tok=>{
      if(tok==null){
        res.json({'success':false,'error':'Invalid Token'});
      }else if(tok.expiration_date < new Date()){
        res.json({'success':false,'error':'Invalid Token'});
      }else{
        User.findOne({where:{id:decode.id,active:true}}).then(user=>{
          if(user==null){
            res.json({'success':false,'error':'Invalid user'});
          }else{
            bcrypt.compare(old_password,user.password,(err,isMatch)=>{
              if(err){
                res.json({'success':false,'error':'Error with password. Please contact admin'});
              }else if(!isMatch){
                res.json({'success':false,'error':'Incorrect Old Password'});
              }else{
                bcrypt.genSalt(10,function(err,salt){
                  bcrypt.hash(new_password,salt,(err,hash)=>{
                    user.update({password:hash}).then(()=>{
                      tok.update({is_valid:false}).then(()=>{
                        res.json({'success':true});
                      });
                    });
                  });
                });
              }
            });
          }
        });
      }
    });
  }
};

exports.register = function(req,res){
  const email = req.body.email;
  const password = req.body.password;
  const first_name = req.body.first_name;
  const last_name = req.body.last_name;
  User.count({where:{email:email,active:true}}).then(count=>{
    if(count!=0){
      res.json({'success':false,'error':'User already exists'});
    }else{
      bcrypt.genSalt(10,function(err,salt){
        bcrypt.hash(password,salt,(err,hash)=>{
          const user = User.build({
            email:email,
            first_name:first_name,
            last_name:last_name,
            password:hash
          });
          user.save().then(task=>{
            res.send({'success':true,'message':'User Saved'});
          }).catch(error=>{
            res.send({'success':false,'error':error});
          });
        });
      });
    }
  });
};

exports.find_user = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const search = req.params.search;
  const offset = parseInt(req.params.offset);
  const limit = 6;
  AuthToken.findOne({where:{token:token,is_valid:true,user_id:decode.id}}).then(r=>{
    if(r==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(r.expiration_date < new Date()){
        r.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        console.log(search);
        sequelize.query("SELECT u.id,u.email,u.first_name,u.last_name FROM users AS u WHERE u.active=1 AND u.email LIKE ? AND u.id!=? LIMIT ?, ?;",
        {replacements:[search+'%',decode.id,offset,limit],type:sequelize.QueryTypes.SELECT}).then(results=>{
          if(results.length==0){
            res.json({'success':false,'error':'No results found'});
          }else{
            var ar = [];
            var counter = 0;
            results.forEach((obj)=>{
              Picture.findOne({where:{user_id:obj.id,deleted:false,is_profile_pic:true}}).then(pic=>{
                if(pic==null){
                  Friend.findOne({where:{owner_id:decode.id,friend_id:obj.id,deleted:false}}).then(friend=>{
                    if(friend==null){
                      FriendReq.findOne({where:{owner_id:decode.id,friend_id:obj.id,deleted:false,confirmed:false}}).then(friend_req=>{
                        if(friend_req==null){
                          FriendReq.findOne({where:{owner_id:obj.id,friend_id:decode.id,deleted:false,confirmed:false}}).then(fr=>{
                            if(fr==null){
                              const add = {
                                id:obj.id,
                                email:obj.email,
                                first_name:obj.first_name,
                                last_name:obj.last_name,
                                profile_pic:null,
                                friends:false,
                                friend_req_sent:false,
                                friend_req_received:false
                              };
                              ar.push(add);
                              counter++;
                              if(counter==results.length){
                                res.json({'success':true,results:ar});
                              }
                            }else{
                              const add = {
                                id:obj.id,
                                email:obj.email,
                                first_name:obj.first_name,
                                last_name:obj.last_name,
                                profile_pic:null,
                                friends:false,
                                friend_req_sent:false,
                                friend_req_received:true
                              };
                              ar.push(add);
                              counter++;
                              if(counter==results.length){
                                res.json({'success':true,results:ar});
                              }
                            }
                          });
                        }else{
                          const add = {
                            id:obj.id,
                            email:obj.email,
                            first_name:obj.first_name,
                            last_name:obj.last_name,
                            profile_pic:null,
                            friends:false,
                            friend_req_sent:true,
                            friend_req_received:false
                          };
                          ar.push(add);
                          counter++;
                          if(counter==results.length){
                            res.json({'success':true,results:ar});
                          }
                        }
                      });
                    }else{
                      const add = {
                        id:obj.id,
                        email:obj.email,
                        first_name:obj.first_name,
                        last_name:obj.last_name,
                        profile_pic:null,
                        friends:true,
                        friend_req_sent:false,
                        friend_req_received:false
                      };
                      ar.push(add);
                      counter++;
                      if(counter==results.length){
                        res.json({'success':true,'results':ar});
                      }
                    }
                  });
                }else{
                  Friend.findOne({where:{owner_id:decode.id,friend_id:obj.id,deleted:false}}).then(friend=>{
                    if(friend==null){
                      FriendReq.findOne({where:{owner_id:decode.id,friend_id:obj.id,deleted:false,confirmed:false}}).then(friend_req=>{
                        if(friend_req==null){
                          FriendReq.findOne({where:{owner_id:obj.id,friend_id:decode.id,deleted:false,confirmed:false}}).then(fr=>{
                            if(fr==null){
                              const add = {
                                id:obj.id,
                                email:obj.email,
                                first_name:obj.first_name,
                                last_name:obj.last_name,
                                profile_pic:pic.url,
                                friends:false,
                                friend_req_sent:false,
                                friend_req_received:false
                              };
                              ar.push(add);
                              counter++;
                              if(counter==results.length){
                                res.json({'success':true,results:ar});
                              }
                            }else{
                              const add = {
                                id:obj.id,
                                email:obj.email,
                                first_name:obj.first_name,
                                last_name:obj.last_name,
                                profile_pic:pic.url,
                                friends:false,
                                friend_req_sent:false,
                                friend_req_received:true
                              };
                              ar.push(add);
                              counter++;
                              if(counter==results.length){
                                res.json({'success':true,results:ar});
                              }
                            }
                          });
                        }else{
                          const add = {
                            id:obj.id,
                            email:obj.email,
                            first_name:obj.first_name,
                            last_name:obj.last_name,
                            profile_pic:pic.url,
                            friends:false,
                            friend_req_sent:true,
                            friend_req_received:false
                          };
                          ar.push(add);
                          counter++;
                          if(counter==results.length){
                            res.json({'success':true,results:ar});
                          }
                        }
                      });
                    }else{
                      const add = {
                        id:obj.id,
                        email:obj.email,
                        first_name:obj.first_name,
                        last_name:obj.last_name,
                        profile_pic:pic.url,
                        friends:true,
                        friend_req_sent:false,
                        friend_req_received:false
                      };
                      ar.push(add);
                      counter++;
                      if(counter==results.length){
                        res.json({'success':true,'results':ar});
                      }
                    }
                  });
                }
              });
            });
          }
        });
      }
    }
  });
}

exports.login = function(req,res){
  const email = req.body.email;
  const password = req.body.password;
  User.findOne({
    where:{email:email,active:true},
    attributes:['password','id','email','first_name','last_name']
  }).then(user=>{
    if(user==null){
      res.json({'success':false,'error':'Username or Password Incorrect'});
    }else{
      bcrypt.compare(password,user.password,(err,isMatch)=>{
        if(err){
            res.send({'success':false,'error':'Error Logging you in. Plase Contact Admin'});
        }
        if(isMatch){
          const d = {id:user.id};
          const token = jwt.sign(d,config.secret,{
            expiresIn: 604800 // 1 week
          });
          AuthToken.findOne({where:{user_id:user.id,is_valid:true}}).then(tok=>{
            if(tok!=null){
              if(tok.expiration_date < new Date()){
                tok.update({is_valid:false}).then(()=>{
                  const auth_token = AuthToken.build({
                    token:token,
                    user_id:user.id
                  });
                  auth_token.save().then(task=>{
                      res.json({
                        'success':true,
                        'message':'User Logged In',
                        'token':token,
                        'user':{
                          id:user.id,
                          first_name:user.first_name,
                          last_name:user.last_name,
                          email:user.email
                        }
                    });
                  });
                });
              }else{
                  res.json({'success':false,'error':'Already Logged In'});
              }
            }else{
              const auth_token = AuthToken.build({
                token:token,
                user_id:user.id
              });
              auth_token.save().then(task=>{
                  res.json({
                    'success':true,
                    'message':'User Logged In',
                    'token':token,
                    'user':{
                      id:user.id,
                      first_name:user.first_name,
                      last_name:user.last_name,
                      email:user.email
                    }
                });
              }).catch(error=>{
                res.json({'success':false,'error':'Error Logging In Plase Contact Administrator.'});
              });
            }
          });

        }else{
          res.json({'success':false,'error':'Username or Password incorrect'});
        }
      });
    }
  });
};

exports.check_token = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{token:token,is_valid:true,user_id:decode.id}}).then(r=>{
    if(r==null){
      res.json({'success':false});
    }else{
      if(r.expiration_date < new Date()){
        res.json({'success':false});
      }else{
        res.json({'success':true});
      }
    }
  });
}

exports.logout = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{token:token,is_valid:true,user_id:decode.id}}).then(r=>{
    if(r==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      r.update({'is_valid':false}).then(()=>{
        res.json({'success':true,'message':'User Logged Out'});
      });
    }
  });
};

exports.user_info = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const user_id = req.params.userId;
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date < new Date() ){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        if(user_id=="me"){
          User.findOne({where:{id:decode.id,active:true}}).then(r=>{
            if(r==null){
              res.json({'success':false,'error':'Invalid User'});
            }else{
              Picture.findOne({where:{user_id:r.id,deleted:false,is_profile_pic:true}}).then(pic=>{
                if(pic==null){
                  res.json({
                    'success':true,
                    'first_name':r.first_name,
                    'last_name':r.last_name,
                    'email':r.email,
                    'id':r.id,
                    'profile_pic':null
                  });
                }else{
                  res.json({
                    'success':true,
                    'first_name':r.first_name,
                    'last_name':r.last_name,
                    'email':r.email,
                    'id':r.id,
                    'profile_pic':pic.url
                  });
                }
              });
            }
          });
        }else{
          User.findOne({where:{id:decode.id,active:true}}).then(r=>{
            if(r==null){
              res.json({'success':false,'error':'Invalid User'});
            }else{
              Picture.findOne({where:{user_id:r.id,deleted:false,is_profile_pic:true}}).then(pic=>{
                if(pic==null){
                  res.json({
                    'success':true,
                    'first_name':r.first_name,
                    'last_name':r.last_name,
                    'email':r.email,
                    'id':r.id,
                    'profile_pic':null
                  });
                }else{
                  res.json({
                    'success':true,
                    'first_name':r.first_name,
                    'last_name':r.last_name,
                    'email':r.email,
                    'id':r.id,
                    'profile_pic':pic.url
                  });
                }
              });
            }
          });
        }
      }
    }
  });
};

// TODO: add forgot password
exports.forgot_password = function(req,res){
  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date < new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        res.json({'success':false,'message':'Not Yet Implemented'});
      }
    }
  });
};

exports.update_info = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date < new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        User.findOne({where:{id:decode.id,active:true}}).then(r=>{
          if(r==null){
            res.json({'success':false,'error':'Invalid User'});
          }else{
            const first_name = req.body.first_name;
            const last_name = req.body.last_name;
            r.update({first_name:first_name,last_name:last_name}).then(()=>{
              res.json({'success':true});
            });
          }
        });
      }
    }
  });
};

exports.delete_user = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date < new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        User.findOne({where:{id:decode.id,active:true}}).then(r=>{
          if(r==null){
            res.json({'success':false,'error':'unexisting user'});
          }else{
            AuthToken.findOne({where:{user_id:decode.id,is_valid:true}}).then(rs=>{
              if(rs==null){
                res.json({'success':false,'error':'Unexisting Auth Token'});
              }else{
                rs.update({'is_valid':false}).then(()=>{
                  r.update({'active':false}).then(()=>{
                    res.json({'success':true,'message':'user deleted'});
                  });
                });
              }
            });
          }
        });
      }
    }
  });
};
