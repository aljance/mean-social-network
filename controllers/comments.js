const {User,Post,FriendReq,Picture,Chat,Comment,Notification,AuthToken,Like,Friend,sequelize,LikeComment} = require('../sequelize');
const jwt = require('jsonwebtoken');

exports.get_all_comments = function(req,res){
  const post_id = parseInt(req.params.postId);
  const offset = parseInt(req.params.offset);
  const limit = 6;

  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];

  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date < new Date()){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      Comment.findAll({
        where:{post_id:post_id,deleted:false},
        limit:limit,
        offset:offset
      }).then(comments=>{
        if(comments.length==0){
          res.json({'success':false,'error':'No comments found for this post'});
        }else{
          var counter=0;
          var ar = [];
          comments.forEach((obj)=>{
            LikeComment.findOne({where:{comment_id:obj.id,active:true,user_id:tok.user_id}}).then(like=>{
              if(like==null){
                User.findOne({where:{active:true,id:obj.user_id}}).then(user=>{
                  if(user==null){
                    const add = {
                      'liked':false,
                      'like_count':obj.like_count,
                      'upload_date':(obj.upload_date).toDateString(),
                      'first_name':null,
                      'last_name':null,
                      'profile_pic':null,
                      'text':obj.text,
                      'id':obj.id,
                      'email':null
                    };
                    counter++;
                    ar.push(add);
                    if(counter==comments.length){
                      res.json({'success':true,'comments':ar});
                    }
                  }else{
                    Picture.findOne({where:{user_id:obj.user_id,is_profile_pic:true,deleted:false}}).then(pic=>{
                      if(pic==null){
                        const add = {
                          'liked':false,
                          'like_count':obj.like_count,
                          'upload_date':obj.upload_date,
                          'first_name':user.first_name,
                          'last_name':user.last_name,
                          'profile_pic':null,
                          'text':obj.text,
                          'id':obj.id,
                          'email':user.email
                        };
                        counter++;
                        ar.push(add);
                        if(counter==comments.length){
                          res.json({'success':true,'comments':ar});
                        }
                      }else{
                        const add = {
                          'liked':false,
                          'like_count':obj.like_count,
                          'upload_date':obj.upload_date,
                          'first_name':user.first_name,
                          'last_name':user.last_name,
                          'profile_pic':pic.url,
                          'text':obj.text,
                          'id':obj.id,
                          'email':user.email
                        };
                        counter++;
                        ar.push(add);
                        if(counter==comments.length){
                          res.json({'success':true,'comments':ar});
                        }
                      }
                    });
                  }
                });
              }else{
                User.findOne({where:{active:true,id:obj.user_id}}).then(user=>{
                  if(user==null){
                    const add = {
                      'liked':true,
                      'like_count':obj.like_count,
                      'upload_date':(obj.upload_date).toDateString(),
                      'first_name':null,
                      'last_name':null,
                      'profile_pic':null,
                      'text':obj.text,
                      'id':obj.id,
                      'email':null
                    };
                    counter++;
                    ar.push(add);
                    if(counter==comments.length){
                      res.json({'success':true,'comments':ar});
                    }
                  }else{
                    Picture.findOne({where:{user_id:obj.user_id,is_profile_pic:true,deleted:false}}).then(pic=>{
                      if(pic==null){
                        const add = {
                          'liked':true,
                          'like_count':obj.like_count,
                          'upload_date':(obj.upload_date).toDateString(),
                          'first_name':user.first_name,
                          'last_name':user.last_name,
                          'profile_pic':null,
                          'text':obj.text,
                          'id':obj.id,
                          'email':user.email
                        };
                        counter++;
                        ar.push(add);
                        if(counter==comments.length){
                          res.json({'success':true,'comments':ar});
                        }
                      }else{
                        const add = {
                          'liked':true,
                          'like_count':obj.like_count,
                          'upload_date':(obj.upload_date).toDateString(),
                          'first_name':user.first_name,
                          'last_name':user.last_name,
                          'profile_pic':pic.url,
                          'text':obj.text,
                          'id':obj.id,
                          'email':user.email
                        };
                        counter++;
                        ar.push(add);
                        if(counter==comments.length){
                          res.json({'success':true,'comments':ar});
                        }
                      }
                    });
                  }
                });
              }
            });
          });
      }
    });
  }
  });


};

exports.create_comment = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const text = req.body.text;
  const post_id = req.body.post_id;

  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date < new Date()){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      User.findOne({where:{id:decode.id,active:true}}).then(user=>{
        if(user==null){
          res.json({'success':false,'error':'User doesnt exists'});
        }else{
          Post.findOne({where:{id:post_id,deleted:false}}).then(post=>{
            if(post==null){
              res.json({'success':false,'error':'Invalid post'});
            }else{
              const comment = Comment.build({user_id:decode.id,text:text,post_id:post_id});
              comment.save().then(()=>{
                post.update({comment_count:post.comment_count+1}).then(()=>{
                  Friend.findAll({where:{owner_id:decode.id,deleted:false}}).then(friends=>{
                    if(friends==null){
                      res.json({'success':true,'message':'comment success but user has no friends'});
                    }else{
                      var counter=0;
                      const txt = "User "+user.first_name+" "+user.last_name+" has commented on a post";
                      if(friends.length==0){
                        res.json({'success':true,'message':'comment success but user has no friends'});
                      }else{
                        friends.forEach((obj)=>{
                          const notif = Notification.build({user_id:obj.owner_id,text:txt});
                          notif.save().then(()=>{
                            console.log('saved');
                          });
                          counter++;
                          if(counter==friends.length){
                            res.json({'success':true,'message':'comment success'});
                          }
                        });
                      }
                    }
                  });
                });
              });
            }
          });
        }
      });
    }
  });



};

exports.update_comment = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const text = req.body.text;
  const comment_id = req.body.comment_id;

  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      Comment.findOne({where:{id:comment_id}}).then(comment=>{
        if(comment==null){
          res.json({'success':false,'error':'comment doesnt exists'});
        }else if(comment.user_id==decode.id){
          comment.update({text:text}).then(()=>{
            res.json({'success':true,'message':'comment updated'});
          });
        }else{
          res.json({'success':false,'error':'Invalid user'});
        }
      });
    }
  });


};

exports.delete_comment = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const comment_id = req.body.comment_id;

  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      Comment.findOne({where:{id:comment_id}}).then(comment=>{
        if(comment==null){
          res.json({'success':false,'error':'comment doesnt exists'});
        }else if(comment.user_id==decode.id){
          comment.update({deleted:true}).then(()=>{
            res.json({'success':true,'message':'comment deleted'});
          });
        }else{
          res.json({'success':false,'error':'Invalid user'});
        }
      });
    }
  });


};

exports.like_comment = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const comment_id = req.body.comment_id;

  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date < new Date()){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      Comment.findOne({where:{id:comment_id,deleted:false}}).then(comment=>{
        if(comment==null){
          res.json({'success':false,'error':'comment doesnt exists'});
        }else{
          LikeComment.findOne({where:{comment_id:comment_id,user_id:decode.id}}).then(like=>{
            if(like==null){
              lk = LikeComment.build({comment_id:comment_id,user_id:decode.id});
              lk.save().then(()=>{
                comment.update({like_count:comment.like_count+1}).then(()=>{
                  res.json({'success':true,'liked':true});
                });
              });
            }else if(like.active==true){
              like.update({active:false}).then(()=>{
                comment.update({like_count:comment.like_count-1}).then(()=>{
                  res.json({'success':true,'liked':false});
                });
              });
            }else{
              like.update({active:true}).then(()=>{
                comment.update({like_count:comment.like_count+1}).then(()=>{
                  res.json({'success':true,'liked':true});
                });
              });
            }
          });
        }
      });
    }
  });
};
