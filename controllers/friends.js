const {User,Post,FriendReq,Picture,Chat,Comment,Notification,AuthToken,Like,Friend} = require('../sequelize');
const jwt = require('jsonwebtoken');

exports.get_all_friends = function(req,res){
  const user_id = req.params.userId;
  const offset = parseInt(req.params.offset,10);
  const limit = 6;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{token:token,is_valid:true,user_id:decode.id}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date<new Date()){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      User.findOne({where:{email:user_id,active:true}}).then(u=>{
        if(u==null){
          res.json({'success':false,'error':'Invalid User'});
        }else{
          Friend.findAll({
            where:{owner_id:u.id,deleted:false},
            offset:offset,
            limit:limit
          }).then(friends=>{
            if(friends.length==0){
              res.json({'success':false,'error':'This user has no friends'});
            }else{
              var counter = 0;
              var ar = [];
              friends.forEach((obj)=>{
                User.findOne({where:{id:obj.friend_id,active:true}}).then(u=>{
                  if(u==null){
                    const add = {
                      'first_name':null,
                      'last_name':null,
                      'email':null,
                      'profile_pic':null,
                      'id':null
                    };
                    ar.push(add);
                    counter++;
                    if(counter==friends.length){
                      res.json({'success':true,'results':ar});
                    }
                  }else{
                    Picture.findOne({where:{user_id:obj.friend_id,deleted:false}}).then(pic=>{
                      if(pic==null){
                        const add = {
                          'first_name':u.first_name,
                          'last_name':u.last_name,
                          'email':u.email,
                          'profile_pic':null,
                          'id':u.id
                        };
                        ar.push(add);
                        counter++;
                        if(counter==friends.length){
                          res.json({'success':true,'results':ar});
                        }
                      }else{
                        const add = {
                          'first_name':u.first_name,
                          'last_name':u.last_name,
                          'email':u.email,
                          'profile_pic':pic.url,
                          'id':u.id
                        };
                        counter++;
                        ar.push(add);
                        if(counter==friends.length){
                          res.json({'success':true,'results':ar});
                        }
                      }
                    });
                  }
                });
              });
            }
          });
        }
      });
    }
  });
};

exports.send_friend_req = function(req,res){
  const friend_id = req.body.friend_id;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  if(friend_id==decode.id){
    res.json({'success':false,'error':'You cant friend yourself'});
  }else{
    AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
      if(tok==null){
        res.json({'success':false,'error':'Invalid Token'});
      }else{
        if(tok.expiration_date < new Date()){
          tok.update({is_valid:false}).then(()=>{
            res.json({'success':false,'error':'Invalid Token'});
          });
        }else{
          User.findOne({where:{id:friend_id,active:true}}).then(user=>{
            if(user==null){
              res.json({'success':false,'error':'User Not Found'});
            }else{
              Friend.findOne({where:{
                friend_id:friend_id,
                owner_id:decode.id,
                deleted:false
              }}).then(friend=>{
                // You are Not Friends
                if(friend==null){
                  FriendReq.findOne({where:{friend_id:friend_id,owner_id:decode.id,deleted:false}}).then(friend_req=>{
                    if(friend_req==null){
                      fr = FriendReq.build({owner_id:decode.id,friend_id:friend_id});
                      fr.save().then(()=>{
                        res.json({'success':true,'message':'Friend Request Sent'});
                      });
                    }else if(friend_req.deleted==true){
                      fr = FriendReq.build({owner_id:decode.id,friend_id:friend_id});
                      fr.save().then(()=>{
                        res.json({'success':true,'message':'Friend Request Sent'});
                      });
                    }else if(friend_req.confirmed==false){
                      res.json({'success':false,'error':'Friend Request Already Sent'});
                    }else if(friend_req.confirmed==true){
                      res.json({'success':false,'error':'You are already friends with this person'});
                    }
                  });
                }else if(friend.deleted==true){
                  fr = FriendReq.build({owner_id:decode.id,friend_id:friend_id});
                  fr.save().then(()=>{
                    res.json({'success':true,'message':'Friend Request Sent'});
                  });
                }else if(friend.deleted==false){
                  res.json({'success':false,'error':'You Are already friends with this person'});
                }
              });
            }
          });
        }
      }
    });
  }
};

exports.accept_friend_req = function(req,res){
  const friend_id = req.body.friend_id;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date < new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        FriendReq.findOne({where:{friend_id:decode.id,owner_id:friend_id,deleted:false}}).then(friend_req=>{
          if(friend_req==null){
            res.json({'success':false,'error':'Friend Request doesnt exists or has already been deleted'});
          }else if(friend_req.confirmed==false){
            Friend.findOne({where:{owner_id:decode.id,friend_id:friend_id,deleted:false}}).then(friend=>{
              if(friend==null){
                friend_req.update({confirmed:true,deleted:true}).then(()=>{
                  const f = Friend.build({owner_id:decode.id,friend_id:friend_id});
                  f.save().then(()=>{
                    const f1 = Friend.build({owner_id:friend_id,friend_id:decode.id});
                    f1.save().then(()=>{
                      res.json({'success':true,'message':'Friend Request Accepted'});
                    });
                  });
                });
              }
            });
          }else if(friend_req.confirmed==true){
            res.json({'success':false,'error':'Friend Request Already Accepted'});
          }
        });
      }
    }
  });
};

exports.decline_frienq_req = function(req,res){
  const friend_id = req.body.friend_id;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date<new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        FriendReq.findOne({where:{friend_id:decode.id,owner_id:friend_id,deleted:false}}).then(friend_req=>{
          if(friend_req==null){
            res.json({'success':false,'error':'Friend Request Doesnt Exists or has already been accepted'});
          }else if(friend_req.confirmed==false){
            friend_req.update({deleted:true}).then(()=>{
              res.json({'success':true,'message':'Friend Request Declined/Canceled'});
            });
          }else if(friend_req.confirmed==true){
            res.json({'success':false,'error':'Friend Request Already Accepted'});
          }
        });
      }
    }
  });
};

exports.delete_friend = function(req,res){
  const friend_id = req.body.friend_id;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      if(tok.expiration_date < new Date()){
        tok.update({is_valid:false}).then(()=>{
          res.json({'success':false,'error':'Invalid Token'});
        });
      }else{
        Friend.findOne({where:{owner_id:decode.id,friend_id:friend_id,deleted:false}}).then(friend=>{
          if(friend==null){
            res.json({'success':false,'error':'Friend Doesnt Exists'});
          }else{
            friend.update({deleted:true}).then(()=>{
              Friend.findOne({where:{owner_id:friend.friend_id,friend_id:friend.owner_id,deleted:false}}).then(result=>{
                if(result==null){
                  res.json({'success':false,'error':'Please contact sys admin'});
                }else{
                  result.update({deleted:true}).then(()=>{
                    res.json({'success':true,'message':'Friend Deleted'});
                  });
                }
              });
            });
          }
        });
      }
    }
  });
};

exports.get_all_friend_request = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const offset = parseInt(req.params.offset,10);
  const limit = 6;
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date<new Date()){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      FriendReq.findAll({
        where:{friend_id:decode.id,deleted:false,confirmed:false},
        limit:limit,
        offset:offset
      }).then(friend_req=>{
        if(friend_req.length==0){
          res.json({'success':false,'error':'No Friend Request Found'});
        }else{
          var counter = 0;
          var ar = [];
          friend_req.forEach((obj)=>{
            User.findOne({where:{id:obj.owner_id,active:true}}).then(user=>{
              if(user==null){
                const add = {
                  'first_name':null,
                  'last_name':null,
                  'email':null,
                  'profile_pic':null,
                  'id':null
                };
                counter++;
                ar.push(add);
                if(counter==friend_req.length){
                  res.json({'success':true,'friend_req':ar});
                }
              }else{
                Picture.findOne({where:{user_id:user.id,deleted:false,is_profile_pic:true}}).then(pic=>{
                  if(pic==null){
                    const add = {
                      'first_name':user.first_name,
                      'last_name':user.last_name,
                      'email':user.email,
                      'profile_pic':null,
                      'id':user.id
                    };
                    counter++;
                    ar.push(add);
                    if(counter==friend_req.length){
                      res.json({'success':true,'friend_req':ar});
                    }
                  }else{
                    const add = {
                      'first_name':user.first_name,
                      'last_name':user.last_name,
                      'email':user.email,
                      'profile_pic':pic.url,
                      'id':user.id
                    };
                    counter++;
                    ar.push(add);
                    if(counter==friend_req.length){
                      res.json({'success':true,'friend_req':ar});
                    }
                  }
                });
              }
            });
          });
        }
      });
    }
  });
};
