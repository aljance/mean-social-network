const {User,Post,FriendReq,Picture,Chat,Comment,Notification,AuthToken,Like,Friend,sequelize,LikeComment,LikePicture,CommentPic,LikeCommentPic} = require('../sequelize');
const jwt = require('jsonwebtoken');

exports.get_all_pics = function(req,res){
  const user_id = req.params.userId;
  const offset = parseInt(req.params.offset);
  const limit = 6;

  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid token'});
    }else if(tok.expiration_date < new Date()){
      res.json({'success':false,'error':'Invalid token'});
    }else{
      User.findOne({where:{email:user_id,active:true}}).then(user=>{
        if(user==null){
          res.json({'success':false,'error':'Invalid user'});
        }else{
          Picture.findAll({
            where:{user_id:user.id,deleted:false},
            limit:limit,
            offset:offset
          }).then(pic=>{
            if(pic==null){
              res.json({'success':false,'error':'No pics found for user'});
            }else{
              if(pic.length==0){
                res.json({'success':false,'error':'No pics found for user'});
              }else{
                var counter = 0;
                var ar = [];
                var inar = [];
                pic.forEach((obj)=>{
                  if(counter==0){
                    const add = {
                      id:obj.id,
                      pic_url:obj.url
                    };
                    inar.push(add);
                    counter++;
                    if(counter==pic.length){
                      ar.push(inar);
                      res.json({'success':true,'results':ar});
                    }
                  }else if(counter%3==0){
                    ar.push(inar);
                    inar = [];
                    const add = {
                      id:obj.id,
                      pic_url:obj.url
                    };
                    inar.push(add);
                    counter++;
                    if(counter==pic.length){
                      ar.push(inar);
                      res.json({'success':true,'results':ar});
                    }
                  }else{
                    const add = {
                      id:obj.id,
                      pic_url:obj.url
                    };
                    inar.push(add);
                    counter++;
                    if(counter==pic.length){
                      ar.push(inar);
                      res.json({'success':true,'results':ar});
                    }
                  }
                });
              }
            }
          });
        }
      });
    }
  });
};

exports.get_profile_pic = function(req,res){
  const user_id = parseInt(req.params.userId);
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      Picture.findOne({where:{user_id:user_id,is_profile_pic:true,deleted:false}}).then(pic=>{
        if(pic==null){
          res.json({'success':false,'error':'This user has no profile pic'});
        }else{
          res.json({'success':true,'profile_pic':pic});
        }
      });
    }
  });
};

exports.set_profile_pic = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const pic_id = req.body.pic_id;
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date < new Date()){
      tok.update({is_valid:false}).then(()=>{
        res.json({'success':false,'error':'Invalid Token'});
      });
    }else{
      Picture.findOne({where:{id:pic_id,deleted:false}}).then(pic=>{
        if(pic==null){
          res.json({'success':false,'error':'Invalid Picture'});
        }else{
          if(pic.user_id!=decode.id){
            res.json({'success':false,'error':'Invalid User'});
          }else{
            pic.update({is_profile_pic:true}).then(()=>{
              res.json({'success':true,'message':'Profile picture set'});
            });
          }
        }
      });
    }
  });

};

exports.create_pic = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const url = req.body.pic_url;

  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      const pic = Picture.build({user_id:decode.id,url:url});
      User.findOne({where:{id:decode.id,active:true}}).then(user=>{
        if(user==null){
          res.json({'success':false,'error':'Invalid user'});
        }else{
          pic.save().then(()=>{
            Friend.findAll({where:{owner_id:decode.id,deleted:false}}).then(friends=>{
              if(friends.length==0){
                res.json({'success':true,'message':'image uploaded but user has no friends'});
              }else{
                var counter=0;
                const txt = "User "+user.first_name+" "+user.last_name+" has uploaded a new picture";
                friends.forEach((obj)=>{
                  const notif = Notification.build({user_id:obj.owner_id,text:txt});
                  notif.save().then(()=>{
                    console.log('saved');
                  });
                  counter++;
                  if(counter==friends.length){
                    res.json({'success':true,'message':'picture upload success'});
                  }
                });
              }
            });
          });
        }
      });
    }
  });
};

exports.delete_pic = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const pic_id = req.body.pic_id;

  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date<new Date()){
      tok.update({is_valid:false}).then(()=>{
        res.json({'success':false,'error':'Invalid Token'});
      });
    }else{
      Picture.findOne({where:{id:pic_id,deleted:false}}).then(pic=>{
        if(pic==null){
          res.json({'success':false,'error':'pic doesnt exists'});
        }else{
          if(pic.user_id==decode.id){
            pic.update({deleted:true}).then(()=>{
              res.json({'success':true,'message':'pic deleted'});
            });
          }else{
            res.json({'success':false,'error':'invalid user'});
          }
        }
      });
    }
  });
};

exports.like_pic = function(req,res){
  const pic_id = req.body.pic_id;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);

  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      Picture.findOne({where:{id:pic_id,deleted:false}}).then(pic=>{
        if(pic==null){
          res.json({'success':false,'error':'pic doesnt exists'});
        }else{
          LikePicture.findOne({where:{pic_id:pic_id,user_id:decode.id}}).then(like=>{
            if(like==null){
              lk = LikePicture.build({user_id:decode.id,pic_id:pic_id});
              lk.save().then(()=>{
                pic.update({like_count:pic.like_count+1}).then(()=>{
                  res.json({'success':true,'message':'pic liked'});
                });
              });
            }else{
              if(like.active==true){
                like.update({active:false}).then(()=>{
                  pic.update({like_count:pic.like_count-1}).then(()=>{
                    res.json({'success':true,'message':'pic disliked'});
                  });
                });
              }else{
                like.update({active:true}).then(()=>{
                  pic.update({like_count:pic.like_count+1}).then(()=>{
                    res.json({'success':true,'message':'pic liked'});
                  });
                });
              }
            }
          });
        }
      });
    }
  });
};

exports.get_pic_comments = function(req,res){
  const pic_id = parseInt(req.params.picId);
  const offset = parseInt(req.params.offset);
  const limit=6;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];

  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      CommentPic.findAll({
        where:{pic_id:pic_id,deleted:false},
        limit:limit,
        offset:offset
      }).then(pic_comments=>{
        if(pic_comments==null){
          res.json({'success':false,'error':'pic has no comments'});
        }else if(pic_comments.length==0){
          res.json({'success':false,'error':'pic has no comments'});
        }else{
          res.json({'success':true,'pic_comments':pic_comments});
        }
      });
    }
  });
};

exports.comment_pic = function(req,res){
  const pic_id = req.body.pic_id;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const text = req.body.text;

  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      User.findOne({where:{id:decode.id}}).then(user=>{
        if(user==null){
          res.json({'success':false,'error':'User doesnt exists'});
        }else{
          Picture.findOne({where:{id:pic_id,is_valid:true}}).then(pic=>{
            if(pic==null){
              res.json({'success':false,'error':'Pic doesnt exists'});
            }else{
              const comment_pic = CommentPic.build({user_id:decode.id,text:text,pic_id:pic_id});
              comment_pic.save().then(()=>{
                pic.update({comment_count:pic.comment_count+1}).then(()=>{
                  Friend.findAll({where:{owner_id:decode.id,deleted:false}}).then(friends=>{
                    var counter=0;
                    const txt = "User "+user.first_name+" "+user.last_name+" has commented a picture";
                    if(friends.length==0){
                      res.json({'success':true,'message':'comment success but user has no friends'});
                    }else{
                      friends.forEach((obj)=>{
                        const notif = Notification.build({user_id:obj.owner_id,text:txt});
                        notif.save().then(()=>{
                          console.log('saved');
                        });
                        counter++;
                        if(counter==friends.length){
                          res.json({'success':true,'message':'comment success'});
                        }
                      });
                    }
                  });
                });
              });
            }
          });
        }
      });
    }
  });
};

exports.like_comment_pic = function(req,res){
  const comment_pic_id = req.body.comment_pic_id;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);

  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      CommentPic.findOne({where:{id:comment_pic_id,deleted:false}}).then(comment_pic=>{
        if(comment_pic==null){
          res.json({'success':false,'error':'invalid comment'});
        }else{
          LikeCommentPic.findOne({where:{user_id:decode.id,comment_pic_id:comment_pic_id}}).then(like=>{
            if(like==null){
              lk = LikeCommentPic.build({user_id:decode.id,comment_pic_id:comment_pic_id});
              lk.save().then(()=>{
                comment_pic.update({like_count:comment_pic.like_count+1}).then(()=>{
                  res.json({'success':true,'message':'comment liked'});
                });
              });
            }else{
              if(like.active==true){
                like.update({active:false}).then(()=>{
                  comment_pic.update({like_count:comment_pic.like_count-1}).then(()=>{
                    res.json({'success':true,'message':'comment disliked'});
                  });
                });
              }else{
                like.update({active:true}).then(()=>{
                  comment_pic.update({like_count:comment_pic.like_count+1}).then(()=>{
                    res.json({'success':true,'message':'comment liked'});
                  });
                });
              }
            }
          });
        }
      });
    }
  });
};

exports.delete_comment_pic = function(req,res){
  const comment_pic_id = req.body.comment_pic_id;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);

  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      CommentPic.findOne({where:{id:comment_pic_id,deleted:false}}).then(comment_pic=>{
        if(comment_pic==null){
          res.json({'success':false,'error':'Invalid comment id'});
        }else{
          if(comment_pic.user_id==decode.id){
            comment_pic.update({deleted:true}).then(()=>{
              Picture.findOne({where:{id:comment_pic.pic_id,is_valid:true}}).then(pic=>{
                if(pic==null){
                  res.json({'success':false,'error':'pic doesnt exists'});
                }else{
                  pic.update({comment_count:pic.comment_count-1}).then(()=>{
                      res.json({'success':true,'message':'comment deleted'});
                  });
                }
              });
            });
          }else{
            res.json({'success':false,'error':'Invalid user'});
          }
        }
      });
    }
  });
};
