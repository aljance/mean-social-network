const {User,Post,FriendReq,Picture,Chat,Comment,Notification,AuthToken,Like,Friend,sequelize,LikeComment,LikePicture,CommentPic,LikeCommentPic,ChatMessage} = require('../sequelize');
const jwt = require('jsonwebtoken');

exports.get_all_chats = function(req,res){
  const offset = parseInt(req.params.offset);
  const limit = 6;
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      sequelize.query('SELECT c.id,c.sender_id,c.receiver_id,c.deleted FROM chats AS c WHERE sender_id=? OR receiver_id=? LIMIT ?, ?',
    {replacements:[decode.id,decode.id,offset,limit],type:sequelize.QueryTypes.SELECT}).then(r=>{
      if(r.length==0){
        res.json({'success':false,'error':'No chats'});
      }else{
        res.json({'success':true,'chats':r});
      }
    });
    }
  });
};

exports.get_all_friends = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const limit = 6;
  const offset = parseInt(req.params.offset);
  AuthToken.findOne({
    where:{user_id:decode.id,token:token,is_valid:true}
  }).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid token'});
    }else if(tok.expiration_date<new Date()){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      Friend.findAll({
        where:{owner_id:decode.id,deleted:false},
        limit:limit,
        offset:offset
      }).then(friends=>{
        if(friends.length==0){
          res.json({'success':false,'error':'User has no friends'});
        }else{
          var counter = 0;
          var ar = [];
          friends.forEach((obj)=>{
            User.findOne({where:{id:obj.friend_id,active:true}}).then(user=>{
              if(user==null){
                const add = {
                  'first_name':null,
                  'last_name':null,
                  'email':null,
                  'user_id':null,
                  'profile_pic':null,
                  'viewed':false
                };
                ar.push(add);
                counter++;
                if(counter==friends.length){
                  res.json({'success':true,'results':ar});
                }
              }else{
                Picture.findOne({where:{user_id:obj.friend_id,deleted:false,is_profile_pic:true}}).then(pic=>{
                  if(pic==null){
                    const add = {
                      'first_name':user.first_name,
                      'last_name':user.last_name,
                      'email':user.email,
                      'user_id':user.id,
                      'profile_pic':null,
                      'viewed':false
                    };
                    ar.push(add);
                    counter++;
                    if(counter==friends.length){
                      res.json({'success':true,'results':ar});
                    }
                  }else{
                    const add = {
                      'first_name':user.first_name,
                      'last_name':user.last_name,
                      'email':user.email,
                      'user_id':user.id,
                      'profile_pic':pic.pic_url,
                      'viewed':false
                    };
                    ar.push(add);
                    counter++;
                    if(counter==friends.length){
                      res.json({'success':true,'results':ar});
                    }
                  }
                });
              }
            });
          });
        }
      });
    }
  });
};

exports.get_all_message_from_chat = function(req,res){
  const friend_id = parseInt(req.params.friend_id);
  const offset = parseInt(req.params.offset);
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const limit = 6;
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date < new Date()){
      tok.update({is_valid:false}).then(()=>{
        res.json({'success':false,'error':'Invalid Token'});
      });
    }else{
      sequelize.query('SELECT c.id, c.sender_id, c.receiver_id FROM chats AS c WHERE (c.sender_id=? AND c.receiver_id=? AND c.deleted=0) OR (c.sender_id=? AND c.receiver_id=? AND c.deleted=0) LIMIT 1;',
      {replacements:[decode.id,friend_id,friend_id,decode.id],type:sequelize.QueryTypes.SELECT}).then(r=>{
        if(r==null || r.length==0){
          const ch = Chat.build({
            sender_id:decode.id,
            receiver_id:friend_id
          });
          ch.save().then(()=>{
            User.findOne({where:{id:friend_id,active:true}}).then(user=>{
              if(user==null){
                res.json({'success':false,'error':'Invalid user'});
              }else{
                Picture.findOne({where:{is_profile_pic:true,deleted:false,user_id:friend_id}}).then(pic=>{
                  if(pic==null){
                    res.json({
                      'success':true,
                      'first_name':user.first_name,
                      'last_name':user.last_name,
                      'user_id':user.id,
                      'email':user.email,
                      'chat_id':ch.id,
                      'profile_pic':null,
                      'messages':null
                    });
                  }else{
                    res.json({
                      'success':true,
                      'first_name':user.first_name,
                      'last_name':user.last_name,
                      'user_id':user.id,
                      'email':user.email,
                      'chat_id':ch.id,
                      'profile_pic':pic.url,
                      'messages':null
                    });
                  }
                });
              }
            });
          });
        }else{
          ChatMessage.findAll({
            where:{chat_id:r[0].id,deleted:false},
            limit:limit,
            offset:offset,
            order:[['upload_date','ASC']]
          }).then(msg=>{
            if(msg.length==0){
              User.findOne({where:{id:friend_id,active:true}}).then(user=>{
                if(user==null){
                  res.json({'success':false,'error':'Invalid user'});
                }else{
                  Picture.findOne({where:{is_profile_pic:true,deleted:false,user_id:friend_id}}).then(pic=>{
                    if(pic==null){
                      res.json({
                        'success':true,
                        'first_name':user.first_name,
                        'last_name':user.last_name,
                        'user_id':user.id,
                        'email':user.email,
                        'chat_id':r[0].id,
                        'profile_pic':null,
                        'messages':null
                      });
                    }else{
                      res.json({
                        'success':true,
                        'first_name':user.first_name,
                        'last_name':user.last_name,
                        'user_id':user.id,
                        'email':user.email,
                        'chat_id':r[0].id,
                        'profile_pic':pic.url,
                        'messages':null
                      });
                    }
                  });
                }
              });
            }else{
              User.findOne({where:{id:friend_id,active:true}}).then(user=>{
                if(user==null){
                  res.json({'success':false,'error':'invalid user'});
                }else{
                  Picture.findOne({where:{is_profile_pic:true,deleted:false,user_id:friend_id}}).then(pic=>{
                    if(pic==null){
                      var counter =0;
                      var ar = [];
                      msg.forEach((obj)=>{
                        var add = {
                          'upload_date':(obj.upload_date).toDateString(),
                          'text':obj.text
                        };
                        if(obj.sender_id!=decode.id){
                          add['sent']=false;
                        }else{
                          add['sent']=true;
                        }
                        counter++;
                        ar.push(add);
                        if(counter==msg.length){
                          res.json({
                            'success':true,
                            'first_name':user.first_name,
                            'last_name':user.last_name,
                            'user_id':user.id,
                            'email':user.email,
                            'chat_id':r[0].id,
                            'profile_pic':null,
                            'messages':ar
                          });
                        }
                      });
                    }else{
                      var counter =0;
                      var ar = [];
                      msg.forEach((obj)=>{
                        var add = {
                          'upload_date':(obj.upload_date).toDateString(),
                          'text':obj.text
                        };
                        if(obj.sender_id!=decode.id){
                          add['sent']=false;
                        }else{
                          add['sent']=true;
                        }
                        counter++;
                        ar.push(add);
                        if(counter==msg.length){
                          res.json({
                            'success':true,
                            'first_name':user.first_name,
                            'last_name':user.last_name,
                            'user_id':user.id,
                            'email':user.email,
                            'chat_id':r[0].id,
                            'profile_pic':pic.url,
                            'messages':ar
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
};

exports.send_message = function(req,res){
  const text = req.body.text;
  const receiver_id = parseInt(req.body.receiver_id);
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date < new Date()){
      tok.update({is_valid:false}).then(()=>{
        res.json({'success':false,'error':'Invalid Token'});
      });
    }else{
      sequelize.query('SELECT c.id, c.sender_id, c.receiver_id, c.deleted FROM chats AS c WHERE (c.receiver_id = ? AND c.sender_id = ? AND deleted=0) OR (c.receiver_id = ? AND c.sender_id = ? AND deleted=0) LIMIT 1',
      {replacements:[decode.id,receiver_id,receiver_id,decode.id],type:sequelize.QueryTypes.SELECT}).then(chat=>{
        if(chat==null || chat.length==0){
          const ch = Chat.build({sender_id:decode.id,receiver_id:receiver_id});
          ch.save().then(()=>{
            const msg = ChatMessage.build({sender_id:decode.id,chat_id:ch.id,text:text});
            msg.save().then(()=>{
              User.findOne({where:{id:decode.id,active:true}}).then(user=>{
                if(user==null){
                  res.json({'success':true,'message':'message sent'});
                }else{
                  const txt = "User "+user.first_name+" "+user.last_name+" sent a message";
                  const notif = Notification.build({user_id:receiver_id,text:txt});
                  notif.save().then(()=>{
                    res.json({'success':true,'message':'message sent'});
                  });
                }
              });
            });
          });
        }else{
          User.findOne({where:{id:chat[0].receiver_id,active:true}}).then(user=>{
            if(user==null){
              res.json({'success':false,'error':'Invalid User'});
            }else{
              const msg = ChatMessage.build({sender_id:decode.id,chat_id:chat[0].id,text:text});
              msg.save().then(()=>{
                const txt = "User "+user.first_name+" "+user.last_name+" sent a message";
                const notif = Notification.build({user_id:receiver_id,text:txt});
                notif.save().then(()=>{
                  res.json({'success':true,'message':'message sent'});
                });
              });
            }
          });
        }
      });
    }
  });
};
