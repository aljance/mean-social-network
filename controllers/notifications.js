const {User,Post,FriendReq,Picture,Chat,Comment,Notification,AuthToken,Like,Friend,sequelize} = require('../sequelize');
const jwt = require('jsonwebtoken');

exports.get_all_notif = function(req,res){
  const offset = parseInt(req.params.offset);
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const limit = 6;
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else if(tok.expiration_date < new Date()){
      tok.update({is_valid:false}).then(()=>{
        res.json({'success':false,'error':'Invalid Token'});
      });
    }else{
      Notification.findAll({where:{user_id:decode.id,read:false},limit:limit,offset:offset}).then(notif=>{
        if(notif==null){
          res.json({'success':false,'error':'This user has no notifications or are all read'});
        }else{
          var counter = 0;
          var ar = [];
          notif.forEach((obj)=>{
            User.findOne({where:{id:obj.user_id,active:true}}).then(user=>{
              if(user==null){
                const add = {
                  'first_name':null,
                  'last_name':null,
                  'profile_pic':null,
                  'email':null,
                  'notif_id':obj.id,
                  'text':obj.text
                };
                ar.push(add);
                counter++;
                if(counter==notif.length){
                  res.json({'success':true,'results':ar});
                }
              }else{
                Picture.findOne({where:{user_id:obj.user_id,is_profile_pic:true,deleted:false}}).then(pic=>{
                  if(pic==null){
                    const add = {
                      'first_name':user.first_name,
                      'last_name':user.last_name,
                      'profile_pic':null,
                      'email':user.email,
                      'notif_id':obj.id,
                      'text':obj.text
                    };
                    ar.push(add);
                    counter++;
                    if(counter==notif.length){
                      res.json({'success':true,'results':ar});
                    }
                  }else{
                    const add = {
                      'first_name':user.first_name,
                      'last_name':user.last_name,
                      'profile_pic':pic.pic_url,
                      'email':user.email,
                      'notif_id':obj.id,
                      'text':obj.text
                    };
                    ar.push(add);
                    counter++;
                    if(counter==notif.length){
                      res.json({'success':true,'results':ar});
                    }
                  }
                });
              }
            });
          });
        }
      });
    }
  });
};

exports.update = function(req,res){
  const h = req.header('Authorization');
  const split = h.split('bearer ');
  const token = split[1];
  const decode = jwt.decode(token);
  const notif_id = parseInt(req.body.notif_id);
  console.log(notif_id);
  console.log(req.body.notif_id);
  AuthToken.findOne({where:{user_id:decode.id,token:token,is_valid:true}}).then(tok=>{
    if(tok==null){
      res.json({'success':false,'error':'Invalid Token'});
    }else{
      Notification.findOne({where:{id:notif_id,read:false}}).then(notif=>{
        if(notif==null){
          res.json({'success':false,'error':'Invalid notification'});
        }else if(notif.user_id!=decode.id){
          res.json({'success':false,'error':'Invalid user id'});
        }else{
          notif.update({read:true}).then(()=>{
            res.json({'success':true,'message':'notification updated'});
          });
        }
      });
    }
  });
};
